package beans;

import java.io.Serializable;
import java.sql.Date;

public class UserDataBeans implements Serializable {
	private int id;
	private String loginId;
	private String password;
	private String confirmpassword;
	private String name;
	private Date birthdate;
	private Date createDate;
	private Date updateDate;

	public UserDataBeans(int id2, String loginId2, String name2, Date birthDate2, String password2, Date createDate2,
			Date updateDate2) {
		this.name = "";
		this.loginId = "";
		this.password = "";
		this.confirmpassword = "";
		this.birthdate = null;

	}

	public UserDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public Date getBirthDate() {
		return birthdate;
	}

	public void setBirthDate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUpdateUserDataBeansInfo(String parameter, String parameter2, String parameter3, int attribute) {

	}

}
