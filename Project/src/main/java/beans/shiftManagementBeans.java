package beans;

import java.sql.Date;

public class shiftManagementBeans {

	Date date;
	Boolean lessonNumber1;
	Boolean lessonNumber2;
	Boolean lessonNumber3;
	Boolean lessonNumber4;
	int userId;

	public shiftManagementBeans(Date date, Boolean lessonNumber1, Boolean lessonNumber2, Boolean lessonNumber3,
			Boolean lessonNumber4, int userId) {
		this.date = date;
		this.lessonNumber1 = lessonNumber1;
		this.lessonNumber2 = lessonNumber2;
		this.lessonNumber3 = lessonNumber3;
		this.lessonNumber4 = lessonNumber4;
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getLessonNumber1() {
		return lessonNumber1;
	}

	public void setLessonNumber1(Boolean lessonNumber1) {
		this.lessonNumber1 = lessonNumber1;
	}

	public Boolean getLessonNumber2() {
		return lessonNumber2;
	}

	public void setLessonNumber2(Boolean lessonNumber2) {
		this.lessonNumber2 = lessonNumber2;
	}

	public Boolean getLessonNumber3() {
		return lessonNumber3;
	}

	public void setLessonNumber3(Boolean lessonNumber3) {
		this.lessonNumber3 = lessonNumber3;
	}

	public Boolean getLessonNumber4() {
		return lessonNumber4;
	}

	public void setLessonNumber4(Boolean lessonNumber4) {
		this.lessonNumber4 = lessonNumber4;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
