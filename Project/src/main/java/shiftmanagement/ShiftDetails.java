package shiftmanagement;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ShiftScheduleMethodBeans;
import beans.UserDataBeans;
import dao.ShiftScheduleMethodDAO;
import dao.UserDataDAO;

/**
 * Servlet implementation class ShiftDetails
 */
@WebServlet("/ShiftDetails")
public class ShiftDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			int userId = (int) session.getAttribute("userId");
			System.out.println(userId + "success");
			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserIdでユーザーを取得
			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDataDAO.getUserDataBeansByUserId(userId)
					: (UserDataBeans) ShiftManagementHelper.cutSessionAttribute(session, "returnUDB");
			String validationMessage = (String) ShiftManagementHelper.cutSessionAttribute(session, "validationMessage");

			request.setAttribute("validationMessage", validationMessage);
			request.setAttribute("udb", udb);

			String id = request.getParameter("id");
			Date day = java.sql.Date.valueOf(id);

			List<ShiftScheduleMethodBeans> shiftdetail = null;

			shiftdetail = ShiftScheduleMethodDAO.getShiftDetailByDay(day);
			request.setAttribute("shiftDetails", shiftdetail);
			request.getRequestDispatcher(ShiftManagementHelper.SHIFT_DETAILS_PAGE);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shiftdetail.jsp");
		dispatcher.forward(request, response);
	}
}

//確定シフトの詳細確認用、所要時間1時間
