package shiftmanagement;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ManagementMethodBeans;
import dao.ManagementMethodDAO;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int user = (int) session.getAttribute("userId");
		if (user == 0) {
			response.sendRedirect("Login");
			return;
		}
		try {
			ArrayList<ManagementMethodBeans> ManagementList = ManagementMethodDAO.getMonthlySchedule();
			request.setAttribute("ManagementList", ManagementList);
			request.getRequestDispatcher(ShiftManagementHelper.TOP_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
//拡張機能以外完了