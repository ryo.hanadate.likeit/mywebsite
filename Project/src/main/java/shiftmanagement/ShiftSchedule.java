package shiftmanagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.shiftManagementBeans;
import dao.ShiftScheduleMethodDAO;

/**
 * Servlet implementation class ShiftSchedule
 */
@WebServlet("/ShiftSchedule")
public class ShiftSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher(ShiftManagementHelper.SHIFT_SCHEDULE_PAGE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<shiftManagementBeans> lessonList = null;
		int user = (int) session.getAttribute("userId");

		String[] lessonsAtDay = request.getParameterValues("lessons");

		try {
			for (String test : lessonsAtDay) {
				System.out.println(test);
				String[] count = test.split("-");
				String date = count[0];
				String lessonnumber = count[1];
				Boolean checkResult = ShiftScheduleMethodDAO.checkOfduplicate(date, user);
				System.out.println(checkResult);
				if (checkResult == false) {
					lessonList = ShiftScheduleMethodDAO.deleteInsertSchedule(date, lessonnumber, user);
				} else {
					lessonList = ShiftScheduleMethodDAO.updateSchedule(date, lessonnumber, user);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String validationMessage = "";
		if (lessonsAtDay.equals(null)) {
			validationMessage += "勤務希望が指定されておりません<br>";
			session.setAttribute("validationMessage", validationMessage);
			response.sendRedirect("ShiftSchedule");
		}

		if (validationMessage.length() == 0) {
			try {
				validationMessage += "登録が完了しました";
				session.setAttribute("validationMessage", validationMessage);
				request.getRequestDispatcher(ShiftManagementHelper.TOP_PAGE).forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			session.setAttribute("validationMessage", validationMessage);
			response.sendRedirect("Error");
		}
		request.getRequestDispatcher(ShiftManagementHelper.TOP_PAGE).forward(request, response);
	}

}
