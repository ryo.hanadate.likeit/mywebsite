package shiftmanagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDataDAO;

/**
 * Servlet implementation class UserList
 */
@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserList() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<UserDataBeans> userList = UserDataDAO.findAll();
		System.out.println(userList);

		request.setAttribute("userList", userList);
		request.getRequestDispatcher(ShiftManagementHelper.USER_LIST_PAGE).forward(request, response);

	}

}

//UserManagementと同じようにユーザ一覧を出力し、編集確認削除ができるようにする
//所要時間1時間