package shiftmanagement;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;

/**
 * 定数保持、処理及び表示簡略化ヘルパークラス
 *
 * @author r-hanadate
 *
 */
public class ShiftManagementHelper {
	// TOPページ
	static final String TOP_PAGE = "/WEB-INF/jsp/index.jsp";
	// エラーページ
	static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	// ユーザ一覧ページ
	static final String USER_LIST_PAGE = "/WEB-INF/jsp/userList.jsp";
	// ユーザー情報
	static final String USER_DATA_PAGE = "/WEB-INF/jsp/userdata.jsp";
	// ユーザー情報更新確認
	static final String USER_DATA_UPDATE_CONFIRM_PAGE = "/WEB-INF/jsp/userdataupdateconfirm.jsp";
	// ユーザー情報更新完了
	static final String USER_DATA_UPDATA_RESULT_PAGE = "/WEB-INF/jsp/userdataupdateresult.jsp";
	// ログイン
	static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
	// ログアウト
	static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
	// 新規登録
	static final String SIGNUP_PAGE = "/WEB-INF/jsp/signup.jsp";
	// 新規登録入力内容確認
	static final String SIGNUP_CONFIRM_PAGE = "/WEB-INF/jsp/signupconfirm.jsp";
	// 新規登録完了
	static final String SIGNUP_RESULT_PAGE = "/WEB-INF/jsp/signupresult.jsp";
	// シフト管理、確認ページ
	static final String SHIFT_MANAGEMENT_PAGE = "/WEB-INF/jsp/shiftmanagement.jsp";

	static final String SHIFT_DETAILS_PAGE = "/WEB-INF/jsp/shiftdetails.jsp";
	// 勤務希望入力ページ
	static final String SHIFT_SCHEDULE_PAGE = "/WEB-INF/jsp/shiftschedule.jsp";
	// 必要講師数入力ページ
	static final String WORK_SCHEDULE_PAGE = "/WEB-INF/jsp/workschedule.jsp";

	public static ShiftManagementHelper getInstance() {
		return new ShiftManagementHelper();
	}

	/**
	 * 暗号化用ハッシュ関数(SHA-512) 暗号化ハッシュとしてBLAKE3も使ってみたい
	 * 
	 * @param target
	 * @return
	 */
	public static String getSha512(String target) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-512");
			md.update(target.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);
		return test;
	}

	/**
	 * ログインIDのバリデーション
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたらfalthを返す
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}
		return false;
	}
}
