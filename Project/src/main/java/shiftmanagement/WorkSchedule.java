package shiftmanagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.shiftManagementBeans;
import dao.WorkScheduleMethodDAO;

/**
 * Servlet implementation class WorkSchedule
 */

/**
 * Servlet implementation class ShiftSchedule
 */
@WebServlet("/WorkSchedule")
public class WorkSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher(ShiftManagementHelper.WORK_SCHEDULE_PAGE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<shiftManagementBeans> lessonList = null;
		int user = (int) session.getAttribute("userId");

		String[] lessons = request.getParameterValues("lessons");
		String[] howmanypeople = request.getParameterValues("number");
		System.out.println(howmanypeople);
		int arraycount = 0;

		try {
			for (String lessonAtDay : lessons) {
				String[] count = lessonAtDay.split("-");
				String date = count[0];
				String lessonnumber = count[1];
				Boolean checkResult = WorkScheduleMethodDAO.checkOfduplicate(date);

				System.out.println(checkResult);

				String howmanypeopleAtDay = howmanypeople[arraycount];
				if (checkResult == false) {
					lessonList = WorkScheduleMethodDAO.deleteInsertSchedule(date, lessonnumber, howmanypeopleAtDay);
				} else {
					lessonList = WorkScheduleMethodDAO.updateSchedule(date, lessonnumber, howmanypeopleAtDay);
				}
				arraycount++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String validationMessage = "";
		if (howmanypeople.equals(null)) {
			validationMessage += "勤務人数が指定されておりません<br>";
			session.setAttribute("validationMessage", validationMessage);
			response.sendRedirect("WorkSchedule");
		}

		if (validationMessage.length() == 0) {
			try {
				validationMessage += "登録が完了しました";
				session.setAttribute("validationMessage", validationMessage);
				request.getRequestDispatcher(ShiftManagementHelper.TOP_PAGE).forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			session.setAttribute("validationMessage", validationMessage);
			response.sendRedirect("Error");
		}
		request.getRequestDispatcher(ShiftManagementHelper.TOP_PAGE).forward(request, response);
	}

}
