package shiftmanagement;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		UserDataBeans udb = session.getAttribute("udb") != null
				? (UserDataBeans) ShiftManagementHelper.cutSessionAttribute(session, "udb")
				: new UserDataBeans();

		String validationMessage = (String) ShiftManagementHelper.cutSessionAttribute(session, "validationMessage");

		request.setAttribute("udb", udb);

		request.setAttribute("validationMessage", validationMessage);
		request.getRequestDispatcher(ShiftManagementHelper.SIGNUP_PAGE).forward(request, response);
	}
}
