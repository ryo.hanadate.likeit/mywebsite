package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.shiftManagementBeans;

public class ShiftScheduleMethodDAO {

	public static Boolean checkOfduplicate(String date, int userId) throws SQLException, ParseException {

		Connection con = null;
		Boolean checkResult = false;

		try {
			con = DBManager.getConnection();
			String checkOfduplicateSql = "SELECT date, userId FROM shift_schedule WHERE date =" + date + " AND userId ="
					+ userId;
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(checkOfduplicateSql);
			if (rs.next()) {
				String checkDate = rs.getString("date");

				if (!checkDate.equals(null)) {
					checkResult = true;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("checkOfduplicate has been conplited");
		return checkResult;
	}

	public static List<shiftManagementBeans> deleteInsertSchedule(String date, String lessonNumber, int userId)
			throws SQLException, ParseException {

		Connection con = null;
		PreparedStatement st = null;
		List<shiftManagementBeans> scheduleList = new ArrayList<shiftManagementBeans>();

		try {

			con = DBManager.getConnection();
			String insertSql = "INSERT INTO shift_schedule(date, lesson" + lessonNumber + ", userId) VALUES(" + date
					+ ", true, " + userId + ")";
			st = con.prepareStatement(insertSql);

			int rs = st.executeUpdate(insertSql);
			System.out.println("insertSchedule has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return scheduleList;
	}

	public static List<shiftManagementBeans> updateSchedule(String date, String lessonNumber, int userId)
			throws SQLException, ParseException {

		Connection con = null;
		PreparedStatement st = null;
		List<shiftManagementBeans> scheduleList = new ArrayList<shiftManagementBeans>();

		try {

			con = DBManager.getConnection();
			String updateSql = "UPDATE shift_schedule SET lesson" + lessonNumber + " = true WHERE date = " + date
					+ " AND userId = " + userId;
			st = con.prepareStatement(updateSql);

			int rs = st.executeUpdate(updateSql);
			System.out.println("updateSchedule has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return scheduleList;
	}
}
