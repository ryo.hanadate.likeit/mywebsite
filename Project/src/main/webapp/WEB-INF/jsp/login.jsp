<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>login</title>
<link href="css/common.css" rel="stylesheet">
</head>

<body>

	<!-- header -->
	<header>
		<h1>シフト管理ツール</h1>
			<nav class="pc-nav">
				<span>ログイン</span>
			</nav>
	</header>
	<!-- /header -->

	<!-- body -->

	<div class="main-visual">
		<h2>ログイン</h2>
			<div class="container">
				<div class="card card-container">
					<form class="form-signin" action="LoginResult" method="POST">
						<div class="text-icenter">
							<input type="text" name="login_id" value="${inputLoginId}"
								class="form-control" placeholder="ログインID" required autofocus>
							<div class="row"></div>
							<input type="password" name="password" id="inputPassword"
								class="form-control" placeholder="パスワード" required>
							<div class="row"></div>
						</div>
						<!-- /form -->
						<button class="btn btn-lg btn-primary btn-block btn-signin"
							type="submit">ログイン</button>
						<div class="row">
							<div class="col s8 offset-s2">
								<p class="right-align">
									<a href="SignUp">新規登録</a>
								</p>
							</div>
						</div>
					</form>
				</div>
			</div>
	</div>

</body>

</html>