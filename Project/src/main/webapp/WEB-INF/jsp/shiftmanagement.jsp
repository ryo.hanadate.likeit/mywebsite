<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>workschedule</title>
    <link href="css/common.css" rel="stylesheet">
</head>

<body>

    <!-- Header Start -->
    <header class="site-header">
        <div class="wrapper site-header__wrapper">
            <a href="Index" class="brand">シフト管理ツール</a>
            <div>${userId}</div>
            <nav class="nav">
                <ul class="nav__wrapper">
                    <li class="nav__item"><a href="Index">シフト一覧</a></li>
                    <li class="nav__item"><a href="ShiftSchedule">シフト希望登録</a></li>
                    <li class="nav__item"><a href="WorkSchedule">必要講師数登録</a></li>
                    <li class="nav__item"><a href="ShiftManagement">シフト確定登録</a></li>
                    <li class="nav__item"><a href="UserList">勤務者一覧</a></li>
                    <li class="nav__item"><a href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- Header End -->
      <div class="wrapper">
        <h2 id="header"></h2>
        <div class="next-prev-button">
            <button id="prev" onclick="prev()">‹</button>
            <button id="next" onclick="next()">›</button>
        </div>
        <div id="span1"></div>
		<form name="form1" method="POST" action="ShiftSchedule">
        	<div id="calendar"></div>
			
       		<script>
            	const lessons = ["日付", "1限", "2限", "3限", "4限"];
            	const today = new Date();
            	var showDate = new Date(today.getFullYear(), today.getMonth(), 1);
            	window.onload = function () {
            	    showProcess(today, calendar);
            	}
            	function prev() {
            	    showDate.setMonth(showDate.getMonth() - 1);
            	    showProcess(showDate);
            	}
            	function next() {
            	    showDate.setMonth(showDate.getMonth() + 1);
            	    showProcess(showDate);
            	}
            	function showProcess(date) {
             	   var year = date.getFullYear();
             	   var month = date.getMonth();
             	   document.querySelector('#header').innerHTML = year + "年 " + (month + 1) + "月";

                   var calendar = createProcess(year, month);
                   document.querySelector('#calendar').innerHTML = calendar;
            	}
            	function createProcess(year, month) {
             	   var calendar = "<table><tr class='dayOfWeek'>";
 					for (var i = 0; i < lessons.length; i++) {
                	    calendar += "<th>" + lessons[i] + "</th>";
                	}
                	calendar += "</tr>";
                	
                	var count = 0;
                	var startDayOfWeek = new Date(year, month, 1).getDay();
                	var endDate = new Date(year, month + 1, 0).getDate();
                	var lastMonthEndDate = new Date(year, month, 0).getDate();
                	var row = Math.ceil((startDayOfWeek + endDate) / lessons.length);
                	var dayOfWeek = startDayOfWeek;
                	for (var i = 0; i < endDate; i++) {
                		if(dayOfWeek === 7){
                			dayOfWeek = dayOfWeek - 7;
                		}

                    	var dayOfWeekStr = [ "日", "月", "火", "水", "木", "金", "土" ][dayOfWeek] ;
                    	var thisMonth = month +1;
                	    calendar += "<tr>";
                	    count++;
                	    calendar += "<td>" + count + "    " + dayOfWeekStr + "</td>";	
               	    	for (var j = 1; j < lessons.length; j++){
                    		calendar += "<td>" 
                    				 + "<label id=\"lesson" 
                    				 + j 
                    				 +  "\">" 
                    				 + "<input type=\"checkbox\" name=\"lessons["
                    				 + count + "][" 
                    				 + j 
                    				 + "]\" id=\"" 
                    				 + j + "\">" 
                    				 + "</label>" 
                    				 + "</td>";
                    	}
               	    	dayOfWeek ++;
                    	calendar += "</tr>";
                    	calendar += "<tr>";
                    	for (var j = 1; j < lessons.length; j++){
                    		calendar += "<td></td>";
                    	}
                    	calendar += "</tr>";
                	}
                	return calendar;
            	}

        	</script>
        	<div class="row">
				<button type="submit" onclick="clickBtn1()" >登録</button>
			</div>
			<script>
  				function clickBtn1() {
  					
    				var lessonsOfDay = [];
   					var lessons = document.form1.lessons;
					var count = document.form1.count;	
    				for (let i = 1; i < count; i++) {
    					for (let j = 0; j < lessons.length; j++){
    						if (lessons[j].checked) {
        						lessonsOfDay.push("1");
      						}else{
      							lessonsOfDay.push("0");
      						}
    					}
    				}
    				document.getElementById("span1").textContent = arr1;
    				return arr1;
  				}
			</script>
			</form>
    </div>
</body>

</html>
<!--二次元行列で表現すること
	ex
	[日付[１限、２限、３限、４限]]
	それぞれのデータをユーザ、日付で紐付けてSQL登録-->