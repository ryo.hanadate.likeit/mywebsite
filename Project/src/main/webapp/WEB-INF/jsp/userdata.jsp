<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>index</title>
    <link href="css/common.css" rel="stylesheet">
</head>

<body>

    <!-- Header Start -->
    <header class="site-header">
        <div class="wrapper site-header__wrapper">
            <a href="Index" class="brand">シフト管理ツール</a>
            <nav class="nav">
                <button class="nav__toggle" aria-expanded="false" type="button">
                    menu
                </button>
                <ul class="nav__wrapper">
                    <li class="nav__item"><a href="Index">シフト一覧</a></li>
                    <li class="nav__item"><a href="ShiftSchedule">シフト希望登録</a></li>
                    <li class="nav__item"><a href="WorkSchedule">必要講師数登録</a></li>
                    <li class="nav__item"><a href="ShiftManagement">シフト確定登録</a></li>
                    <li class="nav__item"><a href="UserData">勤務者一覧</a></li>
                    <li class="nav__item"><a href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- Header End -->

    <form>
		<div class="row justify-content-center" style="padding: 30;">
			<h2>ユーザー情報詳細参照</h2>
		</div>
		<div class="table">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">項目</th>
						<th scope="col">情報</th>
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<th scope="row">ログインID</th>
						<td>${udbinfo.loginId}</td>

					</tr>
					<tr>
						<th scope="row">ユーザ名</th>
						<td>${udbinfo.name}</td>

					</tr>
					<tr>
						<th scope="row">生年月日</th>
						<td></td>

					</tr>
					<tr>
						<th scope="row">登録日時</th>
						<td></td>

					</tr>
					<tr>
						<th scope="row">更新日時</th>
						<td></td>
					</tr>
               <tr>
						<th scope="row">担当可能科目</th>
						<td></td>

					</tr>
               <tr>
						<th scope="row">備考</th>
						<td></td>

					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="row">
			<a href="Index"> 戻る </a>
		</div>
	</form>
</body>
</html>