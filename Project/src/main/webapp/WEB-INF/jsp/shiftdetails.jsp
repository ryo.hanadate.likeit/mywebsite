<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>index</title>
    <link href="css/common.css" rel="stylesheet">
</head>

<body>

    <!-- Header Start -->
    <header class="site-header">
        <div class="wrapper site-header__wrapper">
            <a href="Index" class="brand">シフト管理ツール</a>
            <div>${userId}</div>
            <nav class="nav">
                <ul class="nav__wrapper">
                    <li class="nav__item"><a href="Index">シフト一覧</a></li>
                    <li class="nav__item"><a href="ShiftSchedule">シフト希望登録</a></li>
                    <li class="nav__item"><a href="WorkSchedule">必要講師数登録</a></li>
                    <li class="nav__item"><a href="ShiftManagement">シフト確定登録</a></li>
                    <li class="nav__item"><a href="UserList">勤務者一覧</a></li>
                    <li class="nav__item"><a href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
   <!-- Header End -->
   <p>10/1 月</p>
   <table>
      <tr>
         <th>講師名</th>
         <th>1限</th>
         <th>2限</th>
         <th>3限</th>
         <th>4限</th>
      </tr>
      <tr>
         <td>Aさん</td>
         <td id="#" name="lesson1"></td>
         <td id="#" name="lesson2"></td>
         <td id="#" name="lesson3"></td>
         <td id="#" name="lesson4"></td>
      </tr>
   </table>
</body>

</html>