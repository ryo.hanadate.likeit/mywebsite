<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>index</title>
    <link href="css/common.css" rel="stylesheet">
</head>

<body>

    <!-- Header Start -->
    <header class="site-header">
        <div class="wrapper site-header__wrapper">
            <a href="Index" class="brand">シフト管理ツール</a>
            <div>${loginId}</div>
            <nav class="nav">
                <ul class="nav__wrapper">
                    <li class="nav__item"><a href="Index">シフト一覧</a></li>
                    <li class="nav__item"><a href="ShiftSchedule">シフト希望登録</a></li>
                    <li class="nav__item"><a href="WorkSchedule">必要講師数登録</a></li>
                    <li class="nav__item"><a href="ShiftManagement">シフト確定登録</a></li>
                    <li class="nav__item"><a href="UserList">勤務者一覧</a></li>
                    <li class="nav__item"><a href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- Header End -->

  
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ログインID</th>
							<th>ユーザ名</th>
							<th>生年月日</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<c:forEach var="user" items="${userList}">
							
								<td>${user.loginId}</td>
								<td>${user.name}</td>
								<td>${user.birthDate}</td>
								<!-- TODO 未実装；ログインボタンの表示制御を行う -->

								<td>
									<form>

										<a class="btn btn-primary" href="Details?id=${user.id}">詳細</a>
										<c:if
											test="${userInfo.loginId==user.loginId || userInfo.loginId=='admin'}">

											<a class="btn btn-success" href="Update?id=${user.id}">更新</a>
										</c:if>

										<c:if test="${userInfo.loginId=='admin'}">

											<a class="btn btn-danger" href="Delete?id=${user.id}">削除</a>
										</c:if>

									</form>
								</td>
							
						</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
</body>

</html>