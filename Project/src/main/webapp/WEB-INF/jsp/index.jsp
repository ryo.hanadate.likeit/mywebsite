<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>index</title>
    <link href="css/index.css" rel="stylesheet">
</head>

<body>

    <!-- Header Start -->
    <header class="site-header">
        <div class="wrapper site-header__wrapper">
            <a href="Index" class="brand">シフト管理ツール</a>
            <div>${inputLoginId.inputLoginId}</div>
            <nav class="nav">
                <ul class="nav__wrapper">
                    <li class="nav__item"><a href="Index">シフト一覧</a></li>
                    <li class="nav__item"><a href="ShiftSchedule">シフト希望登録</a></li>
                    <li class="nav__item"><a href="WorkSchedule">必要講師数登録</a></li>
                    <li class="nav__item"><a href="ShiftManagement">シフト確定登録</a></li>
                    <li class="nav__item"><a href="UserList">勤務者一覧</a></li>
                    <li class="nav__item"><a href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- Header End -->

    <div class="wrapper">
        <h2 id="header"></h2>
        <div class="next-prev-button">
            <button id="prev" onclick="prev()">‹</button>
            <button id="next" onclick="next()">›</button>
        </div>

        <div id="calendar"></div>

        <script>
            const week = ["日", "月", "火", "水", "木", "金", "土"];
            const today = new Date();
            var showDate = new Date(today.getFullYear(), today.getMonth(), 1);
            window.onload = function () {
                showProcess(today, calendar);
            }
            function prev() {
                showDate.setMonth(showDate.getMonth() - 1);
                showProcess(showDate);
            }
            function next() {
                showDate.setMonth(showDate.getMonth() + 1);
                showProcess(showDate);
            }
            function showProcess(date) {
                var year = date.getFullYear();
                var month = date.getMonth();
                document.querySelector('#header').innerHTML = year + "年 " + (month + 1) + "月";

                var calendar = createProcess(year, month);
                document.querySelector('#calendar').innerHTML = calendar;
            }
            function createProcess(year, month) {
                var calendar = "<table><tr class='dayOfWeek'>";
                for (var i = 0; i < week.length; i++) {
                    calendar += "<th>" + week[i] + "</th>";
                }
                calendar += "</tr>";

                var count = 0;
                var startDayOfWeek = new Date(year, month, 1).getDay();
                var endDate = new Date(year, month + 1, 0).getDate();
                var lastMonthEndDate = new Date(year, month, 0).getDate();
                var row = Math.ceil((startDayOfWeek + endDate) / week.length);
                for (var i = 0; i < row; i++) {
                    calendar += "<tr>";
                    for (var j = 0; j < week.length; j++) {
                        if (i == 0 && j < startDayOfWeek) {
                            calendar += "<td class='disabled'>" + (lastMonthEndDate - startDayOfWeek + j + 1) + "</td>";
                        } else if (count >= endDate) {
                            count++;
                            calendar += "<td class='disabled'>" + (count - endDate) + "</td>";
                        } else {
                            count++;
                            if (year == today.getFullYear()
                                && month == (today.getMonth())
                                && count == today.getDate()) {
                                calendar += "<td class='today'>" + "<a href=" + "ShiftDetails?id=" + year + "-" + (month + 1) + "-" + count + ">" + count + "</a>" + "</td>";
                            } else {
                                calendar += "<td>" + "<a href=" + "ShiftDetails?id=" + year + "-" + (month + 1) + "-" + count + ">" + count + "</a>" + "</td>";
                            }
                        }
                    }
                    calendar += "</tr>";
                }
                return calendar;
            }

        </script>
    </div>
</body>

</html>