<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>index</title>
    <link href="css/common.css" rel="stylesheet">
</head>

<body>

    <!-- Header Start -->
    <header class="site-header">
        <div class="wrapper site-header__wrapper">
            <a href="Index" class="brand">シフト管理ツール</a>
            <span>${userId}</span>
            <nav class="nav">
                <button class="nav__toggle" aria-expanded="false" type="button">
                    menu
                </button>
                <ul class="nav__wrapper">
                    <li class="nav__item"><a href="Index">シフト一覧</a></li>
                    <li class="nav__item"><a href="ShiftSchedule">シフト希望登録</a></li>
                    <li class="nav__item"><a href="WorkSchedule">必要講師数登録</a></li>
                    <li class="nav__item"><a href="ShiftManagement">シフト確定登録</a></li>
                    <li class="nav__item"><a href="UserData">勤務者一覧</a></li>
                    <li class="nav__item"><a href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- Header End -->
    <div class="wrapper">
        <h2 id="header"></h2>
        <div class="next-prev-button">
            <button id="prev" onclick="prev()">‹</button>
            <button id="next" onclick="next()">›</button>
        </div>
        <div id="span1"></div>
		<form name="form1" method="POST" action="WorkSchedule">
        	<div id="calendar"></div>
			
       		<script>
            	const lessons = ["日付", "1限", "2限", "3限", "4限"];
            	const today = new Date();
            	var showDate = new Date(today.getFullYear(), today.getMonth(), 1);
            	window.onload = function () {
            	    showProcess(today, calendar);
            	}
            	function prev() {
            	    showDate.setMonth(showDate.getMonth() - 1);
            	    showProcess(showDate);
            	}
            	function next() {
            	    showDate.setMonth(showDate.getMonth() + 1);
            	    showProcess(showDate);
            	}
            	function showProcess(date) {
             	   var year = date.getFullYear();
             	   var month = date.getMonth();
             	   document.querySelector('#header').innerHTML = year + "年 " + (month + 1) + "月";

                   var calendar = createProcess(year, month);
                   document.querySelector('#calendar').innerHTML = calendar;
            	}
            	function createProcess(year, month) {
             	   var calendar = "<table><tr class='dayOfWeek'>";
 					for (var i = 0; i < lessons.length; i++) {
                	    calendar += "<th>" + lessons[i] + "</th>";
                	}
                	calendar += "</tr>";
                	
                	var count = 0;
                	var startDayOfWeek = new Date(year, month, 1).getDay();
                	var endDate = new Date(year, month + 1, 0).getDate();
                	var lastMonthEndDate = new Date(year, month, 0).getDate();
                	var row = Math.ceil((startDayOfWeek + endDate) / lessons.length);
                	var dayOfWeek = startDayOfWeek;
                	for (var i = 0; i < endDate; i++) {
                		if(dayOfWeek === 7){
                			dayOfWeek = dayOfWeek - 7;
                		}

                    	var dayOfWeekStr = [ "日", "月", "火", "水", "木", "金", "土" ][dayOfWeek] ;
                	    calendar += "<tr>";
                	    count++;
                	    calendar += "<td class=\"date\">" + count + "    " + dayOfWeekStr + "</td>";
               	    	for (var number = 1; number < lessons.length; number++){
               	    		if(count < 10){
               	    			var arrayOfDate = year + "" + (month + 1) + "0" + count;
               	    		}else{
               	    			var arrayOfDate = year + "" + (month + 1) + "" + count;
               	    		}
                    		calendar += "<td>" + "<label>" + "<input type = \"number\" name = \"number\">" + "<input type=\"hidden\" name=\"lessons\" value=\"" + arrayOfDate + "-" + number + "\">" + "</label>" + "</td>";
                    	}
               	    	dayOfWeek ++;
                    	calendar += "</tr>";
                	}
                	return calendar;
            	}

        	</script>
        	<div class="row">
				<button type="submit" name="action" >登録</button>
			</div>
			</form>
    </div>
</body>

</html>